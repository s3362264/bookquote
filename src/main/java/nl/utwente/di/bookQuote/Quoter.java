package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    // HashMap to store book prices based on ISBN
    private static final Map<String, Double> bookPrices = new HashMap<>();

    // Static initializer to populate the bookPrices map
    static {
        bookPrices.put("1", 10.0);
        bookPrices.put("2", 45.0);
        bookPrices.put("3", 20.0);
        bookPrices.put("4", 35.0);
        bookPrices.put("5", 50.0);
    }

    // Implementation of getBookPrice method
    public double getBookPrice(String isbn) {
        // Check if the ISBN exists in the map, if not, return 0.0
        return bookPrices.getOrDefault(isbn, 0.0);
    }
}
